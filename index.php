<?php
/*
 *      index.php
 *      
 *      Copyright 2014 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
 
session_save_path(dirname(__FILE__)."/tmp");
session_start();
require_once (getcwd()."/includes/global.inc.php");
require_once (getcwd()."/includes/html.inc.php");
load_app();
?>
<!DOCTYPE html>
<html lang="es">
<head>
<title>porra 2018</title>
<meta charset="utf-8" />
<link rel="stylesheet" href="css/site.css" />
<link rel="icon" type="image/png" href="images/favicon.png" sizes="32x32" />
<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Philosopher" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script language="Javascript" src="libjs/jquery-1.11.0.min.js" type="text/javascript"></script>
<script language="Javascript" src="libjs/json2.js" type="text/javascript"></script>
<script language="Javascript" src="js/html.js" type="text/javascript"></script>
<script language="Javascript" src="js/resources.js" type="text/javascript"></script>
<script language="Javascript" type="text/javascript">
$(document).ready(function(){
 init_desktop();
});
</script>	
</head>
<body>
    <header>
       <div id="loading">procesando ... </div>
       <div class="title-and-status">
        <h1 class="main-title">Porra Mundial Rusia 2018</h1>
        <div id="status">
                <?php echo get_html_status();?>
        </div>
       </div>
    <div id="menu">
        <ul>
            <li id="ophome">inicio</li>
            <li id="opmorosos">morosos</li>
            <li id="opupdate">actualizar</li>
            <li id="opnewticket">nueva porra</li>
        </ul>
    </div>
   </header>
    <section>
       <article>
        <div class="dashboard">
            <div id="clasificacion">
                <?php echo get_html_classifications();?>
            </div>
            <div id='morosos'><?php echo get_html_listusers();?></div>
            </div>
       </article>
    </section>
    <aside>
    </aside>
    <footer><a href="https://twitter.com/antiliga">@antiliga</a></footer>
</body>
</html>
