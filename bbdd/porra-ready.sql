-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: porra
-- ------------------------------------------------------
-- Server version	5.6.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `boleto`
--

DROP TABLE IF EXISTS `boleto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boleto` (
  `idparticipante` varchar(255) NOT NULL,
  `idenfrentamiento` int(11) NOT NULL,
  `resultado` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `new_index` (`idparticipante`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boleto`
--

LOCK TABLES `boleto` WRITE;
/*!40000 ALTER TABLE `boleto` DISABLE KEYS */;
/*!40000 ALTER TABLE `boleto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `boletofases`
--

DROP TABLE IF EXISTS `boletofases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boletofases` (
  `idparticipante` varchar(255) NOT NULL,
  `idseleccion` int(11) NOT NULL,
  `idfase` int(11) NOT NULL,
  `idpos` varchar(10) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `new_index` (`idparticipante`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boletofases`
--

LOCK TABLES `boletofases` WRITE;
/*!40000 ALTER TABLE `boletofases` DISABLE KEYS */;
/*!40000 ALTER TABLE `boletofases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enfrentamientos`
--

DROP TABLE IF EXISTS `enfrentamientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enfrentamientos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ids1` int(11) DEFAULT NULL,
  `ids2` int(11) DEFAULT NULL,
  `fase` int(11) NOT NULL,
  `resultado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `new_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enfrentamientos`
--

LOCK TABLES `enfrentamientos` WRITE;
/*!40000 ALTER TABLE `enfrentamientos` DISABLE KEYS */;
INSERT INTO `enfrentamientos` VALUES (1,2,3,1,0),(2,1,4,1,0),(3,6,5,1,0),(4,7,8,1,0),(5,11,9,1,0),(6,13,15,1,0),(7,12,10,1,0),(8,14,16,1,0),(9,18,19,1,0),(10,21,23,1,0),(11,17,20,1,0),(12,24,22,1,0),(13,25,27,1,0),(14,28,26,1,0),(15,31,32,1,0),(16,29,30,1,0),(17,2,1,1,0),(18,7,6,1,0),(19,4,3,1,0),(20,5,8,1,0),(21,11,12,1,0),(22,10,9,1,0),(23,13,14,1,0),(24,17,18,1,0),(25,16,15,1,0),(26,19,20,1,0),(27,25,28,1,0),(28,21,24,1,0),(29,22,23,1,0),(30,26,27,1,0),(31,30,32,1,0),(32,31,29,1,0),(33,3,1,1,0),(34,4,2,1,0),(35,8,6,1,0),(36,5,7,1,0),(37,10,11,1,0),(38,9,12,1,0),(39,15,14,1,0),(40,16,13,1,0),(41,22,21,1,0),(42,23,24,1,0),(43,19,17,1,0),(44,20,18,1,0),(45,32,29,1,0),(46,30,31,1,0),(47,26,25,1,0),(48,27,28,1,0);
/*!40000 ALTER TABLE `enfrentamientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fases`
--

DROP TABLE IF EXISTS `fases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fases` (
  `idseleccion` int(11) NOT NULL,
  `idfase` int(11) NOT NULL,
  `idpos` varchar(10) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fases`
--

LOCK TABLES `fases` WRITE;
/*!40000 ALTER TABLE `fases` DISABLE KEYS */;
/*!40000 ALTER TABLE `fases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participantes`
--

DROP TABLE IF EXISTS `participantes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participantes` (
  `id` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `puntos` int(11) NOT NULL,
  `pagado` int(11) NOT NULL,
  `aciertos` int(11) NOT NULL,
  `fallos` int(11) NOT NULL,
  `fecha` varchar(255) NOT NULL,
  `done` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participantes`
--

LOCK TABLES `participantes` WRITE;
/*!40000 ALTER TABLE `participantes` DISABLE KEYS */;
/*!40000 ALTER TABLE `participantes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pichichi`
--

DROP TABLE IF EXISTS `pichichi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pichichi` (
  `idparticipante` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`idparticipante`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pichichi`
--

LOCK TABLES `pichichi` WRITE;
/*!40000 ALTER TABLE `pichichi` DISABLE KEYS */;
/*!40000 ALTER TABLE `pichichi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selecciones`
--

DROP TABLE IF EXISTS `selecciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selecciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `grupo` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selecciones`
--

LOCK TABLES `selecciones` WRITE;
/*!40000 ALTER TABLE `selecciones` DISABLE KEYS */;
INSERT INTO `selecciones` VALUES (1,'Egipto','A'),(2,'Rusia','A'),(3,'Arabia Saudita','A'),(4,'Uruguay','A'),(5,'Irán','B'),(6,'Marruecos','B'),(7,'Portugal','B'),(8,'España','B'),(9,'Australia','C'),(10,'Dinamarca','C'),(11,'Francia','C'),(12,'Perú','C'),(13,'Argentina','D'),(14,'Croacia','D'),(15,'Islandia','D'),(16,'Nigeria','D'),(17,'Brasil','E'),(18,'Costa Rica','E'),(19,'Serbia','E'),(20,'Suiza','E'),(21,'Alemania','F'),(22,'Corea del Sur','F'),(23,'México','F'),(24,'Suecia','F'),(25,'Bélgica','G'),(26,'Inglaterra','G'),(27,'Panamá','G'),(28,'Túnez','G'),(29,'Colombia','H'),(30,'Japón','H'),(31,'Polonia','H'),(32,'Senegal','H');
/*!40000 ALTER TABLE `selecciones` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-06 13:58:32
