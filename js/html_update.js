/*
 *      html_update.js
 *      
 *      Copyright 2014 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

function do_update_view_phases(){
  //$("#loading").show();
  var currentid;
  var selector;
  $.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 9},
          success:function(data){
				var obj = $.parseJSON(data);
				//FASE DE GRUPOS
				$("section article div#container_phase1 div#matches_phase1 select").each(function() {
					$(this).val(obj[$(this).attr("id")]);
				}); 
				
				//OCTAVOS
				$("section article div#container_phase2 div#matches_phase2 select").each(function() {
					if (obj[$(this).attr("id")]){
						currentid="#"+$(this).attr("id")+" option[value='0']";
						selector="section article div#container_phase2 div#matches_phase2 select"+currentid;
						$(selector).remove();
						$(this).val(obj[$(this).attr("id")]);
					}	
				}); 
				
				//CUARTOS
				$("section article div#container_phase3 div#matches_phase3 select").each(function() {
					if (obj[$(this).attr("id")]){
						currentid="#"+$(this).attr("id")+" option[value='0']";
						selector="section article div#container_phase3 div#matches_phase3 select"+currentid;
						$(selector).remove();
						$(this).val(obj[$(this).attr("id")]);
					}	
				});
				
				//SEMIFINALES
				$("section article div#container_phase4 div#matches_phase4 select").each(function() {
					if (obj[$(this).attr("id")]){
						currentid="#"+$(this).attr("id")+" option[value='0']";
						selector="section article div#container_phase4 div#matches_phase4 select"+currentid;
						$(selector).remove();
						$(this).val(obj[$(this).attr("id")]);
					}	
				});
				
				//FINAL
				$("section article div#container_phase5 div#matches_phase5 select").each(function() {
					if (obj[$(this).attr("id")]){
						currentid="#"+$(this).attr("id")+" option[value='0']";
						selector="section article div#container_phase5 div#matches_phase5 select"+currentid;
						$(selector).remove();
						$(this).val(obj[$(this).attr("id")]);
					}	
				});
				
				//CAMPEONES
				if (obj["1"])
					$("section article div#container_champions div#champions_results select").val(obj["1"]);
				else
					$("section article div#container_champions div#champions_results select").val(0);
				/*$("section article div#container_champions div#champions_results select").each(function() {
					if (obj[$(this).attr("id")]){
						currentid="#"+$(this).attr("id")+" option[value='0']";
						selector="section article div#container_champions div#champions_results select"+currentid;
						$(selector).remove();
						$(this).val(obj[$(this).attr("id")]);
					}	
				});*/ 
				$("section article div input#btnupdate").bind("click",function (){
              do_set_update_ticket();
            });
            $("section article div input#btncancel").bind("click",function (){
              window.location.href="index.php";
            });
			}
  });
}

function do_set_update_ticket(){
	$("section article div input#btnupdate").attr("value","procesando ...");
	$("section article div input#btnupdate").unbind("click");
	$("#loading").show();
	var String1X2="";
	var phase2String="";
	var phase3String="";
	var phase4String="";
	var phase5String="";
	var campeon="";
	
	var Obj1X2=make_update_json_obj("section article div#container_phase1 select");
	if (Obj1X2) String1X2 =JSON.stringify(Obj1X2);
	
	var phase2Obj=make_update_json_obj("section article div#container_phase2 select");
	if (phase2Obj) phase2String =JSON.stringify(phase2Obj);
	var phase3Obj=make_update_json_obj("section article div#container_phase3 select");
	if (phase3Obj) phase3String =JSON.stringify(phase3Obj);
	
	var phase4Obj=make_update_json_obj("section article div#container_phase4 select");
	if (phase4Obj) phase4String =JSON.stringify(phase4Obj);
	
	var phase5Obj=make_update_json_obj("section article div#container_phase5 select");
	if (phase5Obj) phase5String =JSON.stringify(phase5Obj);

//	if ($("section article div#container_champions div#champions_results select#campeon").val()!=0)
	campeon=$("section article div#container_champions div#champions_results select#campeon").val();
	
	$.ajax({
			 type: 'POST',
			 url: "ajax/ajaxhtml.php",
			 data: {opt: 10, phase1: String1X2, phase2: phase2String, phase3: phase3String, phase4: phase4String, phase5: phase5String, champion:campeon},
			 success:function(html){
				$("#loading").hide();
				alert("Clasificación actualizada de forma correcta");
				window.location.href="index.php";
			 }
	 });
	
}
