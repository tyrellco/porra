/*
 *      resources.js
 *      
 *      Copyright 2014 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
 
 function init_desktop(){
  $("#loading").hide();
  update_menu_state("ophome");
  $("header div#menu ul li#ophome").bind("click",function (){
    window.location.href="index.php";
  });
  
  $("header div#menu ul li#opnewticket").bind("click",function (){
	update_menu_state("opnewticket");
	init_new_ticket();
  });
  
  $("header div#menu ul li#opmorosos").bind("click",function (){
	 update_menu_state("opmorosos");
    do_pre_get_html_defaulter();
  });
  
  $("header div#menu ul li#opupdate").bind("click",function (){
    do_pre_go_update();
  });
  
  $("section article div#clasificacion table tr td.linkimg i[id^='zoom_']").bind("click",function (){
	 var viduser = $(this).attr("id").split("_"); 
    get_html_ticket(viduser[1]);
  });
  
 }
 
function update_menu_state(id){
	$("header div#menu ul li").css("border-bottom","0px");
	if (id=="opnewticket")
		$("header div#menu ul li#"+id).css("border-top","5px solid #3181E1");
	else	
		$("header div#menu ul li#"+id).css("border-bottom","5px solid #9F9A9A");
}

 function do_pre_get_html_defaulter(){
	$.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 15},
          success:function(value){
				 if (value==0)
					do_get_html_auth(1);
				 else{	
					do_get_html_defaulter();
					//update_menu_state("opmorosos","johndoe");
				 }	
          }
  });
}

 function do_pre_go_update(){
	$.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 15},
          success:function(value){
			 if (value==0)
				do_get_html_auth(2);
			 else	
				window.location.href="update.php";
          }
  });
}
  
 function init_new_ticket(){
    $("header div#menu ul li#opmorosos, li#opupdate").bind("click",function (){
  	 window.location.href="index.php";
  });
  
	var go;
	var html="<div id='container_register'>\
	<div id='data_user' class='matches'>\
	<h2>Datos personales</h2>\
   <table id='tableregister'>\
	  <tr><th>Nombre:</th><td><input type='text' id='name' /></td></tr>\
	  <tr><th>Apellidos:</th><td><input type='text' id='surname'/></td></tr>\
	</table>\
	</div>\
	<div class='button_bottom_right'><input id='btnregister' type='button' value='continuar >>'/></div>\
	</div>";
	
  $("section article").html(html);
  $("section article").fadeIn();
  $("section article div#container_register input#btnregister").bind("click",function (){
	 go=1; //por defecto continuamos 
	 $("section article div#container_register input[type='text']").each(function (){
		 if ( ($(this).val().match((/^\ /))) || (!$(this).val()) ){
			 alert("Es obligatorio indicar nombre y apellidos. Y tampoco no dejes espacios en blanco al inicio ...");
			 $(this).css("border","2px solid red");
			 go=0; //NO continuamos
			 return false;
		 }
	 }); 
	 if (go)
		do_set_register($("section article div#container_register input#name").val(),$("section article div#container_register input#surname").val());
  });
 } 

function make_json_obj(idcontainer){
	
	var objJSON={};
	$(idcontainer).each(function() {
		if ($(this).val()=='0'){
			$(idcontainer).filter(function( index ) {
				return $(this).val() === "0";
			}).parent().prev().css( "border", "2px solid red" );
				$("#loading").hide();
        objJSON=null;
        alert("Te has dejado algo sin rellenar");
				return false;
		}	
		else
			objJSON[$(this).attr('id')]=$(this).val();
	});
	
	return objJSON;
}

function make_update_json_obj(selector){
	
	var objJSON={};
	$(selector).each(function() {
		if ($(this).val()!='0'){
			objJSON[$(this).attr('id')]=$(this).val();
		}	
	});
	
	return objJSON;
}


function set_bind_change(){
  
	$("section article div#container_phase2 select" ).unbind();
	$("section article div#container_phase2 select" ).bind("change",function (){
    if ($("section article div#container_phase3 select").length){
			$("section article div#container_phase3 div#matches_phase3").fadeOut();  
			do_get_allselect_phase3();
			$("section article div#container_phase3 div#matches_phase3").fadeIn();
		}
  });
	
	$("section article div#container_phase3 select" ).unbind();
	$("section article div#container_phase3 select" ).bind("change",function (){
  	  if ($("section article div#container_phase4 select").length){
			$("section article div#container_phase4 div#matches_phase4").fadeOut();  
			do_get_allselect_phase4();
			$("section article div#container_phase4 div#matches_phase4").fadeIn();
		}
  });
	
	$("section article div#container_phase4 select" ).unbind();
	$("section article div#container_phase4 select" ).bind("change",function (){
    if ($("section article div#container_phase5 select").length){
			$("section article div#container_phase5 div#matches_phase5").fadeOut();  
			do_get_allselect_phase5();
			$("section article div#container_phase5 div#matches_phase5").fadeIn();
		}
  });
}	

function do_get_next(){
  
  var html="<div class='button_bottom_right'><input id='btn2nextphase' type='button' value='continuar >>'/></div>";
    
  return html;

}

function do_get_save(){
  
  var html="<div class='button_bottom_center'><input id='btn2save' type='button' value='aceptar'/></div>";
  
  return html;

}

function do_get_back(){
  
  var html="<div class='button_bottom_center'><input id='btn2back' type='button' value='<< volver'/></div>";
  
  return html;

}

function do_get_ok(){
  
  var html="<div class='button_bottom_center'><input id='btn2ok' type='button' value='aceptar'/></div>";
  
  return html;

}

function do_get_index(){
  
  var html="<div class='button_bottom_center' id='btnindex'><input id='btn2ok' type='button' value='volver'/></div>";
  
  return html;

}

function get_html_container_phases(){
	var html="<div id='all_groups'></div>\
	<div id='container_phase1'>\
	<h2>Fase de grupos</h2>\
	<div id='matches_phase1' class='matches'></div>\
</div>\
<div id='container_phase2'>\
	<h2>Octavos de final</h2>\
	<div id='matches_phase2' class='matches'></div>\
</div>\
<div id='container_phase3'>\
	<h2>Cuartos de final</h2>\
	<div id='matches_phase3' class='matches'></div>\
</div>\
<div id='container_phase4'>\
	<h2>Semifinales</h2>\
	<div id='matches_phase4' class='matches'></div>\
</div>\
<div id='container_phase5'>\
	<h2>Final</h2>\
	<div id='matches_phase5' class='matches'></div>\
</div>\
<div id='container_champions'>\
	<h2>Campeón y Pichichi</h2>\
	<div id='champions_results' class='matches'></div>\
</div>";

return html;
}
