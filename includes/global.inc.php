<?php
/*
 *      global.inc.php
 *      
 *      Copyright 2014 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

require_once(dirname(__FILE__)."/../classes/MySQL.class.php");

function load_app(){
	# cargamos el la configuracion inicial
	$_SESSION["config"]=parse_ini_file(dirname(__FILE__)."/../conf/config.ini",true);

	$oMYSQL=MYSQL::get_instancia();
	$sql="SELECT * FROM selecciones ORDER BY grupo, nombre";	
	
	# Vector de session con todas las selecciones
	if (!isset ($_SESSION["teams"])){
		$team_object=$oMYSQL->get_resource($sql);
		$vteams=array();
		while ($team=mysql_fetch_object($team_object))
			$vteams[$team->id]=$team->nombre;
		$vteams[0]="XXXX";	
		$_SESSION["teams"]=$vteams;
	}
	
	# Matriz de sesion con los grupos
	if (!isset ($_SESSION["groups"])){
		
		$currentgroup="A";
		$vteams=array();
		$vgroups=array();
		mysql_data_seek($team_object, 0);	//ponemos el puntero de nuevo al comienzo del objeto
		while ($team=mysql_fetch_object($team_object)){
			if (strcmp($currentgroup,$team->grupo)){//no coincide el grupo actual con el la tupla
				$vgroups[$currentgroup]=$vteams; //asignamos la selecciones al grupo
				$vteams=array();
				$currentgroup=$team->grupo;
			}
			array_push($vteams,$team->nombre);
		}
		$vgroups[$currentgroup]=$vteams;
		$_SESSION["groups"]=$vgroups;
	}
}
?>
